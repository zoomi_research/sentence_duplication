from collections import Counter
import os


def Levenshtein(s1, s2):
    s1, s2 = s1.strip(), s2.strip()
    if not s1 and not s2:
        return 0
    if not s1:
        return len(s2)
    if not s2:
        return len(s1)

    # convert s1 into s2
    # dynamic programming method to find minimum edit distance between s1 and s2
    row = range(len(s2)+1)
    for i, char_s1 in enumerate(s1):
        new_row = [i+1]
        for j, char_s2 in enumerate(s2):
            new_row.append(min( new_row[j]+1, row[j+1]+1, row[j]+(char_s1!=char_s2)))
        row = new_row
    return row[-1]



class Worker(object):
    def __init__(self, seg_text=None, audio_text=None):
        self.punc = '.!?'
        self.seg_text = seg_text
        self.audio_text = audio_text
        self.leven_thres = 5
        if self.seg_text:
            self.seg_sentences = self.split_text(seg_text)
        if self.audio_text:
            self.audio_sentences = self.split_text(audio_text)

    def load_segment_text(self, seg_text):
        self.seg_text = seg_text
        if self.seg_text:
            self.seg_sentences = self.split_text(seg_text)

    def split_text(self, text):
        sentences = []
        sen = ''
        for char in text:
            if char in self.punc:
                sentences.append(sen.strip())
                sen = ''
            else:
                sen += char
        return sentences


    def count_duplicates(self):
        # unique_sentences = set(self.seg_sentences)
        sentence_counter = Counter(self.seg_sentences)
        sentence_counter = {sen:sentence_counter[sen] for sen in sentence_counter if sentence_counter[sen] > 1}
        return sentence_counter


    def audio_transcript_in_one_string(self, audio_transcript):

        dup, non_dup = [], []

        audio_transcript_word_list = audio_transcript.split()
        num_words_in_audio = len(audio_transcript_word_list)

        for sen in self.seg_sentences:
            sen_length = len(sen.split())
            if sen_length > num_words_in_audio:
                continue
            for start_ind in range(0, num_words_in_audio-sen_length+1):
                distance = self.fuzzy_match(sen, ' '.join(audio_transcript_word_list[start_ind:start_ind+sen_length]))
                if distance < self.leven_thres:
                    dup.append(sen)
                else:
                    non_dup.append(sen)

        return dup, non_dup



    def audio_transcript_in_sentences(self, audio_sentences):
        # remove any audio sentence that is an exact match to a slide text sentence
        audio_sentences = [a_sen for a_sen in audio_sentences if a_sen not in self.seg_sentences]
        return audio_sentences

    def audio_transcript_in_sentences(self, audio_sentences):
        # remove any audio sentence that is an fuzzy match to a slide text sentence
        dup, non_dup = [], []
        for a_sen in audio_sentences:
            for slide_sen in self.seg_sentences:
                if Levenshtein(a_sen, slide_sen) < self.leven_thres:
                    dup.append(a_sen)
                else:
                    non_dup.append(a_sen)

        return dup, non_dup


if __name__ == '__main__':
    # s1 = 'extension'
    # s2 = 'extension'
    # print Levenshtein(s1, s2)
    # exit()

    W = Worker()
    directory = 'PDG_OLP'
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            File_object = open(os.path.join(directory, filename), "r")
            lines = File_object.readlines()
            s = ' '.join([line.rstrip('\n') for line in lines])

            print(os.path.join(directory, filename))

            W.load_segment_text(s)
            sentence_counter = W.count_duplicates()
            for sen in sentence_counter:
                print sentence_counter[sen], '     ', sen
            print '---------------'






