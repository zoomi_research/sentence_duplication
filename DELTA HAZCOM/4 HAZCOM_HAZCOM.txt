   Rectangle 5 Rectangle Rectangle Rectangle 5 Rectangle 1 Rectangle 2 delta_logo.png Group
 Group
 Group
 Group
 Group
 Group
 Group
 Group
 audio-normal.png Oval Oval Acute vs. Chronic Effects 

There is a difference between acute effects and chronic effects of exposure to chemicals. back-normal.png Oval Hotspot 1 next-normal.png Oval 1 y6teu_i1.jpg Acute chemical effects are due to a sudden overexposure; for example, a firefighter overcome by smoke. Chronic chemical effects are often the result of low levels of exposure over long periods of time. Lung cancer caused by long-term use of cigarettes, is an example of a chronic effect.
Here are some important points to understand about the dose concept in regard to the toxic effects of chemicals:
Every substance we come in contact with is potentially harmful, or toxic.
The degree of toxicity depends upon the dose (how much and for how long you're exposed). HAZCOM 03  of  34 Oval 2 Chevron 1 Oval 1 Chevron 2 Oval 3 Line 1 Line 2 Oval 4 Triangle 1 Oval 5 ? Oval 6 Group
 6 Triangle 3 Rectangle 3 Rectangle 4 Oval 7 BlockArc 1 Triangle 2 Oval 8 Multiply 1 audio-normal.png audio-mute.png back-normal.png back-disabled.png next-normal.png next-disabled.png Rectangle 7urf5_img_item_1.jpg Actual or Suspected Spillage/Leakage

Every actual or suspected spillage or leakage of a Dangerous Goods shipment (cargo or checked baggage) must be assumed to be an emergency until proven otherwise. X Rectangle 7urf5_img_item_2.jpg Notify Appropriate Authorities

Under all circumstances, appropriate airport, police, fire, or other disaster agencies must be immediately notified for instructions and assistance. Except for obvious actions such as evacuating personnel and isolating spilled materials, get your immediate supervisor involved to handle the more uncommon items. X

there is a difference between acute effects and chronic effects of exposure to chemicals 