   Rectangle 5 Rectangle 5 Rectangle 1 Rectangle 2 delta_logo.png Group
 Group
 Group
 Group
 Group
 Group
 Group
 Group
 audio-normal.png Oval Oval back-normal.png Oval Hotspot 1 next-normal.png Oval 1 Secondary Labeling System If you pour chemicals into another container, such as a generic spray bottle, you must label the second container appropriately. This label is called a Secondary Container Label and has different requirements from the Primary Container Label. 
Although secondary labeling systems vary slightly between airlines, most are similar. Delta and Delta Connection carriers use warning codes and identified handling procedures. This system is called the Hazardous Materials Identification System (HMIS).
Pre-printed labels can be ordered for deicing materials and some cabin service materials. Secondary labels for all workplace chemicals may be printed from the Workplace Chemical List (WCL). Blank labels are also available. Blanks must be filled in with label codes from the WCL or Safety Data Sheet (SDS).
A label guide explaining HMIS codes is available on bulletin boards in areas where chemicals are used and in the Environmental Programs Manual (EPM).
Click here to review the label guide.
 g3zkk_i1.jpg HAZCOM 10  of  34 Oval 2 Chevron 1 Oval 1 Chevron 2 Oval 3 Line 1 Line 2 Oval 4 Triangle 1 Oval 5 ? Oval 6 Group
 6 Triangle 3 Rectangle 3 Rectangle 4 Oval 7 BlockArc 1 Triangle 2 Oval 8 Multiply 1 audio-normal.png audio-mute.png back-normal.png back-disabled.png next-normal.png next-disabled.png

if you pour chemicals into another container such as a generic spray bottle you must label the second container appropriately this label is called a secondary container label and has different requirements from the primary container label 