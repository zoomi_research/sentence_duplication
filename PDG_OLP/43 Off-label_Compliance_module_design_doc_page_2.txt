SOLUTION AND SCOPE

PDG will work with Company Medical to update two (2) Compliance courses – Off-Label 
Promotion and AdvaMed and Anti-Kickback. Work on this program will begin April 12 with a 
target completion date of July 20.

Scope:
§ Deliverables include: 

§

Off-Label Promotion 40-minute e-learning course

§ Stock graphics, or graphics provided by client

§ Up to 4 custom graphics

§ Up to 30 minutes of narration (up to 2 voices)

§ Standard platform-native interactions (no custom interactions)

§

20 question assessment with scoring

§

AdvaMed and Anti-Kickback 60-minute e-learning course –same as above, except:

§ Up to 6 custom graphics

§ Up to 45 minutes of narration (up to 2 voices)

© Copyright 2018 Performance Development Group LLC
© Copyright 2018 Performance Development Group LLC

