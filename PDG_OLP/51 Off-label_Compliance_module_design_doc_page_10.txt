Content Overview –AdvaMed and AKS (cont’d)

§ Scenario involving a 
Company employee 
planning an education 
conference

Narrator explains AdvaMed and 
Company policy on:
§ Training and educating Healthcare 

Professionals

§ Supporting Healthcare 

Professionals through independent 
and educational conferences
§ Sales, promotion and other 

business meetings

Narrator explains:
§ Resources and Next 

§ The employee is trying to 
decide which venue is best

§ The employee picks a venue 
that is modest and reasonable

§ Engaging a particular Healthcare 

Steps

Professional as a consultant

§ Assessment

Scenario - Intro

2 MIN

2 MIN
Policy 

Compliance

2 MIN

Scenario – Part 2

AdvaMed

2 MIN

AdvaMed and 
Company Policy

12 MIN

5 MIN

Scenarios

Closing and 
Assessment

10 MIN

Narrator explains:
§ Why this employee has to 
choose carefully based on 
company policy

§ More to come

Narrator introduces AdvaMed
and Code of Ethics

§ 2 to 3 Scenarios with 

feedback (2 moderately 
challenging; 1 more 
challenging) 

§ Learner helps characters 

choose between two options

Total Seat Time: ~60minutes

