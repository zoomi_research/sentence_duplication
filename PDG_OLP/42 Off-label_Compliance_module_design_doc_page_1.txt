COMPLIANCE MODULE UPDATES
MASTER DESIGN STRATEGY DOCUMENT

PDG Project Code: GLM3025

•
• Master Design Strategy Document (MDSD)
• May 4, 2018

CONFIDENTIAL.© Performance Development Group LLC 2018.  All Rights Reserved.  Printed in the U.S.A.  This document, including any writing, drawings, notes or 
verbal representation made or shown in the course of these communications is confidential and proprietary to Performance Development Group LLC.  No part of the 

materials included in this communication should be: a) reproduced; b) published in any form by any means, electronic or mechanical, including photocopying or 
information storage or retrieval system; or c) disclosed to any third parties, without the express written authorization of Performance Development Group LLC.

Performance Development Group |   610.854.4400   |   performdev.com

© Copyright 2018 Performance Development Group LLC

